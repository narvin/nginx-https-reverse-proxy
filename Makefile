include .env

COMPOSE_FILE = src/docker-compose.yaml
FRONTEND_HTTP_CONF = src/conf/template/frontend.http.conf.template
FRONTEND_HTTPS_CONF = src/conf/template/frontend.https.conf.template
UP = NGINX_FRONTEND_HOST="${NGINX_FRONTEND_HOST}" \
	NGINX_BACKEND_HOST="${NGINX_BACKEND_HOST}" \
	NGINX_BACKEND_PROTOCOL="${NGINX_BACKEND_PROTOCOL:-https}" \
	NGINX_BACKEND_PORT="${NGINX_BACKEND_PORT:-8443}" \
	docker-compose -f $(COMPOSE_FILE) up
DOWN = docker-compose -f $(COMPOSE_FILE) down
EXEC = docker exec -it
LS = docker ps -a

.PHONY: up down exec http https

up:
	@$(UP)
	@$(LS)

down:
	@$(DOWN)
	@$(LS)

exec:
	@$(EXEC) src_nginx_1 bash

http:
	@[ -f $(FRONTEND_HTTPS_CONF) ] \
		&& mv $(FRONTEND_HTTPS_CONF) $(FRONTEND_HTTPS_CONF).disable \
		|| true
	@[ -f $(FRONTEND_HTTP_CONF).disable ] \
		&& mv $(FRONTEND_HTTP_CONF).disable $(FRONTEND_HTTP_CONF) \
		|| true

https:
	@[ -f $(FRONTEND_HTTP_CONF) ] \
		&& mv $(FRONTEND_HTTP_CONF) $(FRONTEND_HTTP_CONF).disable \
		|| true
	@[ -f $(FRONTEND_HTTPS_CONF).disable ] \
		&& mv $(FRONTEND_HTTPS_CONF).disable $(FRONTEND_HTTPS_CONF) \
		|| true
