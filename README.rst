nginx-jenkins
=============

A Dockerized nginx server with certbot that reverse proxies a backend server. The
nginx frontend server block is HTTPS with a Let's Encrypt certificate, and the
backend can be HTTP or HTTPS with either a self-signed or a trusted certificate.

Usage
-----

Create a Dummy Certificate to Drop Wrong Hosts
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If there are other hosts that resolve to the IP that the nginx server is listening
on, then HTTPS requests for those hosts would normally be handled by the first server
block listening on port 443, which could be the frontend server block. To avoid this,
a default server block is defined for port 443 that drops all requests. However,
this server block needs a dummy certificate and key named `dummy.crt` and `dummy.key`
that are located in the `asset` directory.

Generate the dummy key and certficate.

.. code-block:: shell

  openssl genrsa -out asset/dummy.key 4096
  openssl req -new -x509 \
      -days 3650 \
      -key asset/dummy.key \
      -out asset/dummy.crt \
      -subj '/CN=dummy'

Start the nginx Container
~~~~~~~~~~~~~~~~~~~~~~~~~

Start the container from the `src` directory. `NGINX_BACKEND_PORT` and
`NGINX_BACKEND_HOST` must be specified. `NGINX_BACKEND_PROTOCOL` defaults to
`https`. `NGINX_BACKEND_PORT` defaults to `8443`.

.. code-block:: shell

  cd src
  NGINX_FRONTEND_HOST=frontend.example.com \
  NGINX_BACKEND_HOST=192.168.1.22 \
  NGINX_BACKEND_PROTOCOL=https \
  NGINX_BACKEND_PORT=8443 \
  docker-compose up

You may instead specify the values for the `NGINX_*` variables in a `.env` file placed
in the project root directory, and use `make` from the project root directory. See
`env.example` for an example `.env` file.

.. code-block:: shell

  make up

Exec into the container.

.. code-block:: shell

  docker exec -it src_nginx_1 bash

You may instead use `make`.

.. code-block:: shell

  make exec

Create a Let's Encrypt Certificate
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The container mounts the host directory `asset/letsencrypt` to `/etc/letsencrypt`
where nginx expects all Let's Encrypt certificates to reside. Initially, the frontend
server block will be HTTP, and the `letscencrypt` directory will be empty because
no certificates have been created.

From within the container, create a certificte for the frontend.

.. code-block:: shell

  certbot --nginx

Follow the cerbot prompts to create a certificate for the frontend in the `letsencrypt`
directory. This directory will also be persisted on the host in the `asset` directory.

Swith the Frontend Server to HTTPS Mode
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

From project root directory, stop nginx.

.. code-block:: shell

  make down

Disable the frontend HTTP server block, and enable the HTTPS server block.

.. code-block:: shell

  make https

Restart nginx.

.. code-block:: shell

  make up
